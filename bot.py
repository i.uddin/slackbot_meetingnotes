import os
import time
from datetime import datetime
import logging
from slackclient import SlackClient
from typing import List
import pypandoc
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

BOT_ACCESS_TOKEN = os.environ.get('BOT_ACCESS_TOKEN')
TIME_INTERVAL = os.environ.get('TIME_INTERVAL')
CHANNEL_NAME = os.environ.get('SLACK_CHANNEL_NAME')
LOG_DIR = os.environ.get('LOG_DIR')
OUTPUT_DIR = os.environ.get('OUTPUT_DIR')
CHANNEL_ID = None
BOT_ID = None
SLACK_CLIENT = SlackClient(BOT_ACCESS_TOKEN)

# Set up logging and handlers
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
stream = logging.StreamHandler()
file_log = logging.FileHandler(
    '{}/slack_bot_meeting_notes.log'.format(LOG_DIR)
)
stream.setLevel(logging.WARNING)
file_log.setLevel(logging.INFO)

# Set up logging formatters
stream_format = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
filelog_format = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
stream.setFormatter(stream_format)
file_log.setFormatter(filelog_format)

# Add logging handlers
logger.addHandler(stream)
logger.addHandler(file_log)


def find_channel_id(channels: List[dict], channel_name: str) -> str:
    """
    Finds the specified channel from list of channels.

    Args:
        channels (List[dict]): List of dictionaries, each for a channel in
            workspace
        channel_name (str): Channel name to find ID for

    Returns:
        str: Channel ID
    """
    channel_id = None
    for channel in channels:
        if channel['name'] == channel_name:
            logger.info(
                'Channel ID successfully found: {}'.format(channel['id'])
            )
            channel_id = channel['id']

    if not channel_id:
        logger.warning('Unable to find specified channel {}'.format(
            channel_name
        ))
    else:
        return channel_id


def parse_events(event_list: List[dict]) -> None:
    """
    Parses each event received by Slack bot.

    Args:
        event_list (List[dict]): List of Slack events

    Returns:
        None:
    """
    for event in event_list:
        if event["type"] == "message":
            if "bot_id" not in event.keys() and 'text' in event.keys():
                handle_event(event)
                logger.info('Message parsed')


def handle_event(event: dict) -> None:
    """
    Outputs .rst file for message parsed.

    Args:
        event (dict): Dictionary of Slack event received by bot.

    Returns:
        None
    """
    if event["channel"] == CHANNEL_ID:
        try:
            meeting_text = event['text']
            rst = pypandoc.convert_text(meeting_text, 'rst', format='md')

            filename = "{}/{}_daily_meeting.rst".format(
                OUTPUT_DIR,
                datetime.now().strftime("%Y-%m-%d")
            )
            with open(filename, 'w') as file:
                file.write(rst)
                logger.info(
                    'Daily meeting converted and saved to file {}'.format(
                        filename
                    )
                )

            SLACK_CLIENT.api_call(
                'chat.postMessage',
                channel=CHANNEL_ID,
                text="Daily meeting notes successfully captured! 🗒 💾"
            )
        except KeyError:
            pass


if __name__ == "__main__":
    if SLACK_CLIENT.rtm_connect(with_team_state=False):
        logger.info('Bot listening for events...')
        channels = SLACK_CLIENT.api_call('channels.list')
        BOT_ID = SLACK_CLIENT.api_call('auth.test')['user_id']
        CHANNEL_ID = find_channel_id(
            channels['channels'],
            CHANNEL_NAME
        )
        logger.info('Looking for events in channel {}'.format(
            CHANNEL_NAME
        ))
        while True:
            incoming_events = SLACK_CLIENT.rtm_read()
            parse_events(incoming_events)
            time.sleep(int(TIME_INTERVAL))
