# MeetingNotes Slack bot
Slack bot to capture meeting notes published to channel, and convert them into `.RST` files.

## Installation
```bash
pip install requirements.txt
```

## Usage
```bash
python bot.py
```
